FROM alpine:latest

RUN apk --no-cache add dnsmasq

COPY root/ /

EXPOSE 53 53/udp

ENTRYPOINT ["/startup.sh"]
